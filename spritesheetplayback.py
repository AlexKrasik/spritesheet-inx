# !/usr/bin/env python

try:
	import tkinter as tk
except:
	import Tkinter as tk

from lxml import etree
import subprocess
import threading
import tempfile
import inkex
import time
import copy

# :P
class Point():
	def __init__(self, x, y):
		self.x = x
		self.y = y

class SpritesheetPlayback(inkex.Effect):

	# Temp files
	sprite_svg_file = tempfile.gettempdir() + "/__spritesheet.svg__"
	sprite_png_file = tempfile.gettempdir() + "/__spritesheet.png__"
	
	# Try to be uniq id (' ~'; )
	mega_id = "__new_group_just_for_spritesheet_animation_extension_in_inkspape__"

	# Frame number in seq
	frame_index = 0

	def __init__(self):
		inkex.Effect.__init__(self)
		self.OptionParser.add_option('--frame_scale', action = 'store', type = 'int', dest = 'frame_scale', default = '1')
		self.OptionParser.add_option('--frame_width', action = 'store', type = 'int', dest = 'frame_width', default = '100')
		self.OptionParser.add_option('--frame_height', action = 'store', type = 'int', dest = 'frame_height', default = '100')
		self.OptionParser.add_option('--fps', action = 'store', type = 'int', dest = 'fps', default = '60')
		self.OptionParser.add_option('--seq', action = 'store', type = 'string', dest = 'seq', default = '')
	# __init__
			
	def effect(self):
		# Copy args in vars
		self.frame_scale = self.options.frame_scale
		self.dpi = self.frame_scale * 90
		self.frame_width = self.options.frame_width * self.frame_scale
		self.frame_height = self.options.frame_height * self.frame_scale
		self.fps = self.options.fps
		if self.options.seq  == "":
			self.seq = self.options.seq
		else:
			self.seq = self.options.seq.split(",")
		# Make new layer
		doc = self.document.getroot()
		new_layer = etree.XML('<g id = "' + self.mega_id + '" />')
		doc.append(new_layer)
		# Copy selected elements to new layer
		for selected_elem_id in self.options.ids:
			element = copy.copy(self.getElementById(selected_elem_id))
			new_layer.append(element)
		# Save temp svg file
		stream = open(self.sprite_svg_file, 'w')
		stream.write(etree.tostring(doc))
		stream.close()
		# Remove temp layer
		doc.remove(new_layer)
		# Export spritesheet to png
		subprocess.Popen(["inkscape", 
			"--without-gui", 
			"--export-id-only",  
			"--export-id", self.mega_id, 
			"--export-png", self.sprite_png_file, 
			"--export-dpi", str(self.dpi), 
			"--file", self.sprite_svg_file],
			stdout = subprocess.PIPE, 
			stderr = subprocess.PIPE).wait()
		# Main window
		self.root_window = tk.Tk(className = 'preview')
		# Sprite init
		self.sprite = tk.PhotoImage(file = self.sprite_png_file)
		self.sprite.anchor = Point(0, 0)
		# Canvas to draw a Sprite
		self.container = tk.Canvas(width = self.frame_width, height = self.frame_height, master = self.root_window)
		self.container.grid(row = 1, column = 1, columnspan = 3)
		# Adding sprite to canvas and safe ref to img_obj
		self.img_obj = self.container.create_image(self.sprite.width() / 2.0, self.sprite.height() / 2.0, image = self.sprite)
		# Setting label for "Play" button
		self.btn_play_label = tk.StringVar()
		self.btn_play_label.set("PAUSE")
		# Add buttons
		btn_prev = tk.Button(master = self.root_window, command = self.prev_frame, text = "<").grid(row = 2, column = 1)
		btn_play = tk.Button(master = self.root_window, command = self.play_stop, textvariable = self.btn_play_label).grid(row = 2, column = 2)
		btn_next = tk.Button(master = self.root_window, command = self.next_frame, text = ">").grid(row = 2, column = 3)
		# Create & start thread for playback
		self.playback_thread = threading.Thread(target = self.playback_loop)
		self.playback_thread.needWork = True
		self.playback_thread.pause = False
		self.playback_thread.start()
		# Add all selected frames if sequence is empty
		if self.seq == "":
			self.seq = range((self.sprite.width() // self.frame_width) * (self.sprite.height() // self.frame_height))
		# Keyboard binds
		self.root_window.bind("<Key>", self.keyboard_hook)
		# If window close
		self.root_window.protocol("WM_DELETE_WINDOW", self.exit)
		# Start Tkinter
		self.root_window.mainloop()
	# effect

	def play_stop(self):
		self.playback_thread.pause = not self.playback_thread.pause
		if self.playback_thread.pause:
			self.btn_play_label.set("PLAY")
		else:
			self.btn_play_label.set("PAUSE")
	# play_stop

	def prev_frame(self, needPause = True):
		self.playback_thread.pause = True
		self.btn_play_label.set("PLAY")
		self.frame_index -=  1
		if self.frame_index < 0:
			self.frame_index = len(self.seq) - 1
		self.move_to_frame(self.seq[self.frame_index])
	# prev_frame

	def next_frame(self, needPause = True):
		if needPause:
			self.playback_thread.pause = True
			self.btn_play_label.set("PLAY")
		self.frame_index +=  1
		if self.frame_index >=  len(self.seq):
			self.frame_index = 0
		self.move_to_frame(self.seq[self.frame_index])
	# next_frame

	def playback_loop(self):
		while True:
			# Check trigger to stop thread
			if not self.playback_thread.needWork:
				break
			else:
				self.next_frame(False)
				time.sleep(1.0 / self.fps)
			# Wait to unpause
			while self.playback_thread.pause: 
				time.sleep(0.1)
	# playback_loop

	def move_to_frame(self, frame_number):
		x = int(frame_number) % (self.sprite.width() // self.frame_width) * self.frame_width - 1
		y = (int(frame_number) // (self.sprite.width() // self.frame_width)) * self.frame_height - 1
		self.container.move(self.img_obj, self.sprite.anchor.x - x , self.sprite.anchor.y-y)
		self.sprite.anchor.x = x;
		self.sprite.anchor.y = y;
		self.root_window.wm_title("frame: " + frame_number)
	# move_to_frame

	def keyboard_hook(self, event):
		# SPACE key
		if(event.keycode == 65): 
			self.play_stop()
		# LEFT_ARROW key
		if(event.keycode == 113): 
			self.prev_frame()
		# RIGHT_ARROW key
		if(event.keycode == 114): 
			self.next_frame()
	# keyboard_hook

	def exit(self):
		# Set trigger to stop thread
		self.playback_thread.needWork = False;
		self.playback_thread.pause = False;
		# Wait for thread work is over
		while self.playback_thread.isAlive():
			time.sleep(0.1)
		# And close window 
		self.root_window.destroy()
	# exit

e = SpritesheetPlayback()
e.affect()
