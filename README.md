## README ##

# What is it #
This is an extension which can play spritesheet animation right from Inkscape  
Video demo:  
[![0.jpg](https://bitbucket.org/repo/zK6qMz/images/1600267000-0.jpg)](http://youtube.com/watch?v=necRAQzQhu8)

# How to install #
1. Install "python-tk" (tkinter)
2. Copy spritesheet.inx & spritesheet.py to your inkscape extensions directory (ex. /usr/share/inkscape/extensions/)

# How to use #
1. Select your spritesheet elements
2. Go to Extensions --> Spritesheet --> Animation preview
3. Setup up your frame width and height, animation speed (fps) and frame sequence seperated with coma (ex. 0,1,3,2...)
4. Click Apply
5. PROFIT!

# WARNING #
1. This extension tested only with linux (elementary os)
2. Extension has a pretty shitty code style (i'm actual pretty shitty programmer), so if you mental weak developer: don't look at "spritesheet.py". :)